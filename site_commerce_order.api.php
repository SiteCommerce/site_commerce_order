<?php

/**
 * @file
 * Hooks provided by the site_commerce_order module.
 */

/**
 * Allows modules to create an array with default values for fields in the order form.
 */
function site_account_default_order_information_alter(array &$order_information, Drupal\user\Entity\User $account) {}
