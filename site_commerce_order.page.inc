<?php

/**
 * @file
 * Contains site_commerce_order.page.inc.
 *
 * Page callback for Order entities.
 */

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Render\Element;

/**
 * Implements template_preprocess_site_commerce_order().
 */
function template_preprocess_site_commerce_order(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['site_commerce_order'] = $variables['elements']['#site_commerce_order'];
  $order = $variables['site_commerce_order'];

  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Загружает массив с данными корзины.
  // TODO: Нужно убрать реализацию вызова товаров через сервис. Массив надо заполнять через hook.
  $databaseCart = \Drupal::service('site_commerce_cart.database');
  $variables['products'] = $databaseCart->loadCart($order->id());
}

/**
 * Implements template_preprocess_site_commerce_order_checkout().
 */
function template_preprocess_site_commerce_order_checkout(&$variables) {
  $databaseCart = \Drupal::service('site_commerce_cart.database');
  if (!$databaseCart->hasPositionsInCart()) {
    $url = \Drupal\Core\Url::fromRoute('site_commerce_cart.cart_page')->toString();
    $response = new RedirectResponse($url, 302);
    $response->send();
  }

  // Шаблон блока с информацией о составе и стоимости заказа.
  $elements = [
    '#theme' => 'site_commerce_order_checkout_details',
  ];
  $variables['checkout_details'] = $elements;
}

/**
 * Implements template_preprocess_site_commerce_order_checkout_details().
 */
function template_preprocess_site_commerce_order_checkout_details(&$variables) {
  $databaseCart = \Drupal::service('site_commerce_cart.database');
  $variables['positions_in_cart'] = $databaseCart->hasPositionsInCart();
  if ($variables['positions_in_cart']) {
    // Загружает массив с данными корзины.
    $variables['products'] = $databaseCart->loadCart();

    // Шаблон блока с информацией о стоимости заказа на странице подтверждения заказа.
    $elements = [
      '#theme' => 'site_commerce_order_checkout_order_information',
    ];
    $variables['order_information'] = $elements;
  }
}

/**
 * Implements template_preprocess_site_commerce_order_checkout_order_information().
 */
function template_preprocess_site_commerce_order_checkout_order_information(&$variables) {
  $databaseOrder = \Drupal::service('site_commerce_order.database');

  // Загружаем конфигурацию.
  $config = \Drupal::config('site_commerce_order.settings');

  // Определяем стоимость.
  $variables['order_cost'] = $databaseOrder->getOrderCost(0, FALSE);
  $variables['default_currency_code'] = $config->get('default_currency_code') ? $config->get('default_currency_code') : "RUB";

  // Определяем символ валюты.
  $manager = \Drupal::entityTypeManager()
    ->getStorage('site_commerce_currency')
    ->loadByProperties(['letter_code' => $variables['default_currency_code']]);
  $currency = reset($manager);
  $variables['symbol'] = $currency->getSymbol();
}

/**
 * Implements template_preprocess_site_commerce_order_user_orders_dashboard().
 */
function template_preprocess_site_commerce_order_user_orders_dashboard(&$variables) {
  // Загружаем конфигурацию.
  $config = \Drupal::config('site_commerce_order.settings');

  // Текущий пользователь.
  $uid = \Drupal::currentUser()->id();

  // Определяем символ валюты.
  $orders = \Drupal::entityTypeManager()
    ->getStorage('site_commerce_order')
    ->loadByProperties(['owner' => $uid]);

  $variables['orders'] = [];
  $count = 1;
  foreach ($orders as $oid => $order) {
    $variables['orders'][$count] = [
      'order_uuid' => $order->uuid(),
      'number' => $order->getTotalPaidNumber(),
      'symbol' => $order->getTotalPaidCurrencySymbol(),
    ];

    $count++;
  }

  // Путь до каталога товаров.
  $config = \Drupal::config('site_commerce_product.settings');
  $variables['catalog_url'] = empty($config->get('catalog_url')) ? 'catalog' : $config->get('catalog_url');
}

/**
 * Implements template_preprocess_site_commerce_order_confirmed().
 */
function template_preprocess_site_commerce_order_confirmed(&$variables) {
  // Примечание после оформления заказа.
  $config = \Drupal::config('site_commerce_order.settings');
  $variables['order_confirmed_note'] = $config->get('order_confirmed_note');
}

/**
 * Implements template_preprocess_site_commerce_order_view().
 */
function template_preprocess_site_commerce_order_view(&$variables) {
  $databaseCart = \Drupal::service('site_commerce_cart.database');

  // Получаем номер заказа.
  $result = \Drupal::entityTypeManager()
    ->getStorage('site_commerce_order')
    ->loadByProperties(['uuid' => $variables['order_uuid']]);
  $order = reset($result);

  $variables['order'] = [
    'number' => $order->getTotalPaidNumber(),
    'symbol' => $order->getTotalPaidCurrencySymbol(),
  ];

  // Загружает массив с данными корзины.
  $variables['products'] = $databaseCart->loadCart($order->id());
}
