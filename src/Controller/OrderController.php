<?php

namespace Drupal\site_commerce_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_commerce_order\Entity\OrderInterface;

class OrderController extends ControllerBase {

  /**
   * The callback function for order page.
   *
   * @param \Drupal\site_commerce_order\Order $site_commerce_order
   *   The current site_commerce_order.
   *
   * @return string
   *   The page title.
   */
  public function orderTitle(OrderInterface $site_commerce_order) {
    return $this->t('@name', array('@name' => $site_commerce_order->label()));
  }

  /**
   * Страница подтверждения заказа.
   */
  public function checkout() {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_order_checkout',
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Страница подтверждения оформления заказа.
   */
  public function orderConfirmed(string $order_uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_order_confirmed',
      '#order_uuid' => $order_uuid,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Страница просмотра заказа.
   */
  public function view(string $order_uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_order_view',
      '#order_uuid' => $order_uuid,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
