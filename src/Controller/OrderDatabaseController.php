<?php

namespace Drupal\site_commerce_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Database class.
 */
class OrderDatabaseController extends ControllerBase {
  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SiteOrdersDatabaseController.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Проверяет наличие всех позициий в корзине, когда она еще не оформлена в заказ.
   */
  public function hasPositionsInCart() {
    $account = \Drupal::currentUser();

    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->addExpression('COUNT(n.cart_id)');
    $query->condition('n.order_id', 0);

    if ($account->id()) {
      $query->condition('n.uid', $account->id());
    } else {
      $query->condition('n.qrsales_uuid', kvantstudio_user_hash());
    }

    return (bool) $query->execute()->fetchField();
  }

  /**
   * Загружает стоимость заказа по стоимости позиций в корзине.
   */
  public function getOrderCost(int $order_id = 0, bool $trailing_zeros = TRUE) {
    $account = \Drupal::currentUser();
    $query = $this->connection->select('site_commerce_cart', 'n');
    $query->fields('n', array('quantity', 'price__number'));
    $query->condition('n.order_id', $order_id);

    // Определяем позиции добавленные текущим пользователем.
    if (!$order_id) {
      if ($account->id()) {
        $query->condition('uid', $account->id());
      } else {
        $query->condition('qrsales_uuid', kvantstudio_user_hash());
      }
    }

    $result = $query->execute();

    $cost = 0;
    foreach ($result as $row) {
      $cost = $cost + ($row->quantity * $row->price__number);
    }

    // Определяем количество заказов у текущего пользователя.
    // Загружаем конфигурацию.
    $config = \Drupal::config('site_commerce_order.settings');
    $order_discount = (int) $config->get('order_discount');
    if ($order_discount) {
      $orders_quantity = (int) $this->getCurrentUserOrdersQuantity();
      if (!$orders_quantity) {
        $cost = $cost - (($cost * $order_discount) / 100);
      }
    }

    // Убираем незначащие нули.
    if ($trailing_zeros) {
      $cost = round($cost, 2);
      return \Drupal::service('kvantstudio.formatter')->removeTrailingZeros($cost);
    }

    return \Drupal::service('kvantstudio.formatter')->price($cost);
  }

  /**
   * Создает заказ.
   */
  public function createOrder($data) {
    // Текущий пользователь.
    $uid = \Drupal::currentUser()->id();
    $account = \Drupal\user\Entity\User::load($uid);

    // Определяем зарегистрирован клиент в личном кабинете или нет.
    $user_by_phone = FALSE;
    $user_by_mail = FALSE;
    if ($account->isAnonymous()) {
      if (!empty($data['phone_mobile'])) {
        $result = \Drupal::entityTypeManager()
          ->getStorage('user')
          ->loadByProperties(['name' => $data['phone_mobile']]);
        $user_by_phone = reset($result);
      }

      if (!empty($data['mail'])) {
        $result = \Drupal::entityTypeManager()
          ->getStorage('user')
          ->loadByProperties(['name' => $data['mail']]);
        $user_by_mail = reset($result);
      }

      $new_customer = 1;
      if ($user_by_phone || $user_by_mail) {
        $new_customer = 0;
      }
    } else {
      $new_customer = 0;
    }

    // Владелец заказа.
    if (empty($data['owner'])) {
      $owner = $account->id();
    } else {
      $owner = (int) $data['owner'];
    }

    // Загружаем конфигурацию.
    $config = \Drupal::config('site_commerce_order.settings');
    $default_currency_code = $config->get('default_currency_code') ? $config->get('default_currency_code') : "RUB";

    // Идентификатор не авторизованного пользователя.
    $qrsales_uuid = kvantstudio_user_hash();

    // Стоимость заказа.
    $total_paid_number = $this->getOrderCost();

    // Создаем заказ.
    $entity = \Drupal::entityTypeManager()->getStorage('site_commerce_order')->create([
      'type' => 'default',
      'status' => 'new',
      'order_number' => '',
      'store_id' => 0,
      'uid' => $account->id(),
      'owner' => $owner,
      'qrsales_uuid' => $qrsales_uuid,
      'google_uuid' => '',
      'yandex_uuid' => '',
      'new_customer' => $new_customer,
      'total_price' => [
        'number' => $total_paid_number,
        'currency_code' => $default_currency_code,
      ],
      'total_paid' => [
        'number' => $total_paid_number,
        'currency_code' => $default_currency_code,
      ],
      'customer' => $data['name'],
      'telephone' => $data['phone_mobile'],
      'mail' => $data['mail'],
      'hostname' => \Drupal::request()->getClientIp(),
      'delivery_address' => [
        'latitude' => '',
        'longitude' => '',
        'country' => $data['country'],
        'region' => $data['region'],
        'postcode' => $data['postcode'],
        'city' => $data['city'],
        'street' => $data['street'],
        'house_number' => $data['house_number'],
        'flat_number' => $data['flat_number'],
      ],
      'delivery_type' => $data['delivery_type'],
      'payment_method' => $data['payment_method'],
      'comment' => $data['comment'],
      'data' => serialize([]),
      'created' => \Drupal::time()->getRequestTime(),
      'changed' => 0,
      'placed' => 0,
      'completed' => 0,
    ]);
    $entity->enforceIsNew(TRUE);
    $entity->save();

    // Устанавливаем номер заказа.
    $order_number = $this->t('Order number № @value', ['@value' => $entity->id()]);
    $entity->setOrderNumber($order_number);
    $entity->save();

    // Привязка позиций в корзине к заказу.
    $query = $this->connection->update('site_commerce_cart');
    $query->fields([
      'order_id' => $entity->id(),
    ]);
    $query->condition('order_id', 0);
    if ($account->id()) {
      $query->condition('uid', $account->id());
    } else {
      $query->condition('qrsales_uuid', $qrsales_uuid);
    }
    $query->execute();

    return $entity;
  }

  /**
   * Загружает заказ по UUID.
   */
  public function getOrderByUUID(string $uuid) {
    $query = $this->connection->select('site_commerce_order', 'n');
    $query->fields('n');
    $query->condition('n.uuid', $uuid);
    return $query->execute()->fetchObject();
  }

  /**
   * Загружает количество оформленных заказов для текущего пользователя.
   */
  public function getCurrentUserOrdersQuantity() {
    $query = $this->connection->select('site_commerce_order', 'n');
    $query->fields('n');

    // Текущий пользователь.
    $uid = \Drupal::currentUser()->id();
    if ($uid) {
      $query->condition('n.uid', $uid);
    }

    // Идентификатор не авторизованного пользователя.
    if (!$uid) {
      $qrsales_uuid = kvantstudio_user_hash();
      $query->condition('n.qrsales_uuid', $qrsales_uuid);
    }

    return $query->countQuery()->execute()->fetchField();
  }
}
