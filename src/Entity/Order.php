<?php

namespace Drupal\site_commerce_order\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Order entity.
 *
 * @ingroup site_commerce_order
 *
 * @ContentEntityType(
 *   id = "site_commerce_order",
 *   label = @Translation("Order", context = "SiteCommerce"),
 *   label_collection = @Translation("Orders", context = "SiteCommerce"),
 *   label_singular = @Translation("order", context = "SiteCommerce"),
 *   label_plural = @Translation("orders", context = "SiteCommerce"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order",
 *     plural = "@count orders",
 *     context = "SiteCommerce",
 *   ),
 *   bundle_label = @Translation("Order type", context = "SiteCommerce"),
 *   handlers = {
 *     "view_builder" = "Drupal\site_commerce_order\OrderViewBuilder",
 *     "list_builder" = "Drupal\site_commerce_order\OrderListBuilder",
 *     "views_data" = "Drupal\site_commerce_order\Entity\OrderViewsData",
 *     "access" = "Drupal\site_commerce_order\OrderAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_order\Form\OrderForm",
 *       "edit" = "Drupal\site_commerce_order\Form\OrderForm",
 *       "delete" = "Drupal\site_commerce_order\Form\OrderDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_order\OrderHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_order",
 *   admin_permission = "administer site_commerce_order",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "order_id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "owner",
 *   },
 *   links = {
 *     "canonical" = "/admin/site-commerce-order/{site_commerce_order}",
 *     "edit-form" = "/admin/site-commerce/config/orders/{site_commerce_order}/edit",
 *     "delete-form" = "/admin/site-commerce/config/orders/{site_commerce_order}/delete",
 *     "collection" = "/admin/site-commerce/orders",
 *   },
 *   bundle_entity_type = "site_commerce_order_type",
 *   field_ui_base_route = "entity.site_commerce_order_type.edit_form"
 * )
 */
class Order extends ContentEntityBase implements OrderInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Удаляем все позиции из корзины, которые привязаны к заказу.
    if (\Drupal::moduleHandler()->moduleExists('site_commerce_cart')) {
      foreach ($entities as $entity) {
        $query = \Drupal::entityQuery('site_commerce_cart');
        $query->condition('order_id', $entity->id());
        $cart_items = $query->execute();

        $storage_handler = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');
        $cart_entities = $storage_handler->loadMultiple($cart_items);
        $storage_handler->delete($cart_entities);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->get('order_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderNumber() {
    return $this->get('order_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderNumber($order_number) {
    $this->set('order_number', $order_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomer() {
    return $this->get('customer')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isNewCustomer() {
    return (bool) $this->get('new_customer')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalPaidNumber(bool $format = TRUE) {
    $number = $this->get('total_paid')->number;

    if ($format) {
      $number = \Drupal::service('kvantstudio.formatter')->price($number);
    }

    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalPaidСurrencyСode() {
    return $this->get('total_paid')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalPaidCurrencySymbol() {
    $manager = \Drupal::entityTypeManager()
      ->getStorage('site_commerce_currency')
      ->loadByProperties(['letter_code' => $this->getTotalPaidСurrencyСode()]);
    $currency = reset($manager);
    return $currency->getSymbol();
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    $status_code = $this->getStatusCode();
    return site_commerce_order_statuses($status_code);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusCode() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('owner')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('owner')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('owner', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('owner', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreator(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['order_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order number'))
      ->setDescription(t('The order number displayed to the customer.'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['store_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Store'))
      ->setDescription(t('The store to which the order belongs.'))
      ->setCardinality(1)
      ->setRequired(FALSE)
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The user who placed the order.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['qrsales_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID of the unauthorized сustomer'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['google_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The customer ID in google.'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['yandex_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The customer ID in yandex.'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['new_customer'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('New customer'))
      ->setDescription(t('The status of the order being made by a new customer who has not previously made a purchase: 1-the customer is new.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => '0',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['total_price'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Total price'))
      ->setDescription(t('The total paid price of the order.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['total_paid'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Total paid'))
      ->setDescription(t('The total paid price of the order.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['customer'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customer full name'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['telephone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Customer phone'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Customer email'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['hostname'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP address'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setSetting('max_length', 128)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['delivery_address'] = BaseFieldDefinition::create('site_commerce_order_delivery_address')
      ->setLabel(t('Delivery address'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['delivery_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Delivery method'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment_method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Payment method'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order status'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment on the order'))
      ->setRequired(FALSE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the order was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the order was last edited.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['placed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Placed'))
      ->setDescription(t('The time when the order was placed.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Completed'))
      ->setDescription(t('The time when the order was completed.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
}
