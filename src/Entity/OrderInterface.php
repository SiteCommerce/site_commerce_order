<?php

namespace Drupal\site_commerce_order\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Order entities.
 *
 * @ingroup site_commerce_order
 */
interface OrderInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the order number.
   *
   * @return string
   *   Name of the order number.
   */
  public function getOrderNumber();

  /**
   * Gets the order customer.
   *
   * @return string
   *   Name of the customer.
   */
  public function getCustomer();

  /**
   * Gets the order paid cost value.
   *
   * @return string
   *   Paid cost value.
   */
  public function getTotalPaidNumber(bool $format = TRUE);

  /**
   * Gets the order paid currency code value.
   *
   * @return string
   *   Paid currency code value.
   */
  public function getTotalPaidСurrencyСode();

  /**
   * Gets the order paid currency value.
   *
   * @return string
   *   Paid currency value.
   */
  public function getTotalPaidCurrencySymbol();

  /**
   * Sets the order number.
   *
   * @param string $order_number
   *   The order number.
   *
   * @return \Drupal\site_commerce_order\Entity\OrderInterface
   *   The called Order entity.
   */
  public function setOrderNumber($order_number);

  /**
   * Gets the order status.
   *
   * @return string
   *   Order status name.
   */
  public function getStatus();

  /**
   * Gets the order status code value.
   *
   * @return string
   *   Order status code value.
   */
  public function getStatusCode();

  /**
   * Gets the order creation timestamp.
   *
   * @return int
   *   Creation timestamp of the order.
   */
  public function getCreatedTime();

  /**
   * Sets the order creation timestamp.
   *
   * @param int $timestamp
   *   The order creation timestamp.
   *
   * @return \Drupal\site_commerce_order\Entity\OrderInterface
   *   The called Order entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Sets the order creator.
   *
   * @param UserInterface $account
   *   The order creator.
   *
   * @return \Drupal\site_commerce_order\Entity\OrderInterface
   *   The called Order entity.
   */
  public function setCreator(UserInterface $account);

  /**
   * Gets the order customer status.
   *
   * @return bool
   *   Customer status TRUE if is new user.
   */
  public function isNewCustomer();

}
