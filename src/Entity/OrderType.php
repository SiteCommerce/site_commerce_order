<?php
namespace Drupal\site_commerce_order\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Order type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "site_commerce_order_type",
 *   label = @Translation("Order type", context = "SiteCommerce"),
 *   label_collection = @Translation("Order types", context = "SiteCommerce"),
 *   label_singular = @Translation("order type", context = "SiteCommerce"),
 *   label_plural = @Translation("order types", context = "SiteCommerce"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order type",
 *     plural = "@count order types",
 *     context = "SiteCommerce",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\site_commerce_order\OrderTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_order\Form\OrderTypeForm",
 *       "add" = "Drupal\site_commerce_order\Form\OrderTypeForm",
 *       "edit" = "Drupal\site_commerce_order\Form\OrderTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_order\OrderTypeHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site commerce",
 *   config_prefix = "site_commerce_order_type",
 *   bundle_of = "site_commerce_order",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   links = {
 *     "canonical" = "/admin/site-commerce/config/orders/orders-types/{site_commerce_order_type}",
 *     "add-form" = "/admin/site-commerce/config/orders/orders-types/add",
 *     "edit-form" = "/admin/site-commerce/config/orders/orders-types/{site_commerce_order_type}/edit",
 *     "delete-form" = "/admin/site-commerce/config/orders/orders-types/{site_commerce_order_type}/delete",
 *     "collection" = "/admin/site-commerce/config/orders/orders-types",
 *   }
 * )
 */
class OrderType extends ConfigEntityBundleBase {
  /**
   * The entity ID.
   *
   * @var string
   */
  public $id;

  /**
   * The entity label.
   *
   * @var string
   */
  public $label;
}
