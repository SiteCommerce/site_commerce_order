<?php

namespace Drupal\site_commerce_order\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Order entities.
 */
class OrderViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }
}
