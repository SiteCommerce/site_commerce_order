<?php

namespace Drupal\site_commerce_order\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Order entities.
 *
 * @ingroup site_commerce_order
 */
class OrderDeleteForm extends ContentEntityDeleteForm {

}
