<?php

namespace Drupal\site_commerce_order\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class OrderSettingsForm.
 */
class OrderSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_commerce_order.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_order_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Доступные валюты на сайте.
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    // Валюта по умолчанию при добавлении в корзину и оформлении заказа.
    $form['default_currency_code'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Currency'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $config->get('default_currency_code') ? $config->get('default_currency_code') : 'RUB',
      '#description' => new TranslatableMarkup('The default currency when adding to the cart and placing an order.'),
    ];

    // Общие правила использования платежных систем.
    $block_rules_payment_systems = (int) $config->get('block_rules_payment_systems');
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_rules_payment_systems);
    $form['block_rules_payment_systems'] = [
      '#type' => 'entity_autocomplete',
      '#title' => new TranslatableMarkup('Rules for using payment systems'),
      '#target_type' => 'block_content',
      '#default_value' => $block,
      '#description' => new TranslatableMarkup('You need to create a custom block and select it in this field. The block content is displayed above the payment method options. This is the general text for all payment methods.'),
    ];

    // Примечание после оформления заказа.
    $form['order_confirmed_note'] = [
      '#type' => 'textarea',
      '#title' => new TranslatableMarkup('Note after placing an order'),
      '#default_value' => $config->get('order_confirmed_note'),
    ];

    // E-mail для получения информации о новых заказах.
    $form['order_manager_mail'] = [
      '#type' => 'email',
      '#title' => new TranslatableMarkup('E-mail for information about new orders'),
      '#default_value' => $config->get('order_manager_mail'),
    ];

    // Скидка в % при первом оформлении заказа.
    // TODO: данная функция временная до внедрения модуля управления скидками.
    $form['order_discount'] = [
      '#type' => 'number',
      '#title' => 'Скидка в % при первом оформлении заказа',
      '#default_value' => empty($config->get('order_discount')) ? 0 : $config->get('order_discount'),
      '#description' => 'Данная функция временная до внедрения модуля управления акциями и скидками.'
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Сохраняем валюту по умолчанию.
    $default_currency_code = trim($form_state->getValue('default_currency_code'));
    $config->set('default_currency_code', $default_currency_code);

    // Правила использования платежных систем.
    $block_rules_payment_systems = trim($form_state->getValue('block_rules_payment_systems'));
    $config->set('block_rules_payment_systems', $block_rules_payment_systems);

    // Примечание после оформления заказа.
    $order_confirmed_note = trim(strip_tags($form_state->getValue('order_confirmed_note')));
    $config->set('order_confirmed_note', $order_confirmed_note);

    // E-mail для получения информации о новых заказах.
    $order_manager_mail = trim(strip_tags($form_state->getValue('order_manager_mail')));
    $config->set('order_manager_mail', $order_manager_mail);

    // Скидка в % при первом оформлении заказа.
    $order_discount = (int) trim(strip_tags($form_state->getValue('order_discount')));
    $config->set('order_discount', $order_discount);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
