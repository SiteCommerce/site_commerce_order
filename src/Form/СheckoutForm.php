<?php

/**
 * @file
 * Contains \Drupal\site_commerce_order\Form\СheckoutForm.
 */

namespace Drupal\site_commerce_order\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\site_commerce_order\Controller\OrderDatabaseController;
use Drupal\site_commerce_order\Entity\OrderInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send order form.
 */
class СheckoutForm extends FormBase {

  use MessengerTrait;

  /**
   * The servises classes.
   *
   * @var \Drupal\site_commerce_order\Controller\OrderDatabaseController
   */
  protected $databaseOrder;

  /**
   * Default values for form elements.
   *
   * @var array
   */
  protected $default_order_information;

  /**
   * Constructs.
   *
   * @param \Drupal\site_commerce_order\Controller\OrderDatabaseController $connection
   *   The database connection.
   */
  public function __construct(OrderDatabaseController $databaseOrder) {
    $this->databaseOrder = $databaseOrder;

    $uid = \Drupal::currentUser()->id();
    $account = User::load($uid);

    // Информация по умолчанию для формы оформления заказа.
    $this->default_order_information = [
      'delivery_type' => '',
      'country' => '',
      'region' => '',
      'city' => '',
      'postcode' => '',
      'street' => '',
      'house_number' => '',
      'flat_number' => '',
      'name' => $account->getDisplayName(),
      'phone_mobile' => '',
      'mail' => $account->getEmail(),
      'payment_method' => 'site_payments_cash',
    ];
    $order_information = [];
    \Drupal::moduleHandler()->alter('default_order_information', $order_information, $account);
    if (count($order_information)) {
      $this->default_order_information = array_merge($this->default_order_information, $order_information);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_commerce_order.database')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_commerce_order_checkout_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config('kvantstudio.settings');

    // Обертка элементов формы.
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'site-commerce-order-checkout-form__wrapper',
      ],
    ];

    // Адрес получателя.
    $form['wrapper']['container_recipient_address'] = [
      '#type' => 'container',
      '#prefix' => '<div class="site-commerce-order-checkout-form__container">',
      '#suffix' => '</div>',
    ];
    $form['wrapper']['container_recipient_address']['recipient_address_title'] = [
      '#type' => 'item',
      '#markup' => '<div class="site-commerce-order-checkout-form__title site-commerce-order-checkout-form__recipient-address-title">' . $this->t('Recipient address') . '</div>',
    ];

    // Страна получения.
    $form['wrapper']['container_recipient_address']['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['country'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Country')],
      '#required' => FALSE,
      '#suffix' => '<div class="site-commerce-order-checkout-form__country-validation-order"></div>',
    ];

    // Область получения.
    $form['wrapper']['container_recipient_address']['region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['region'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Region')],
      '#required' => FALSE,
      '#suffix' => '<div class="site-commerce-order-checkout-form__region-validation-order"></div>',
    ];

    // Почтовый индекс.
    $form['wrapper']['container_recipient_address']['postcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postcode'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['postcode'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Postcode')],
      '#required' => FALSE,
      '#suffix' => '<div class="site-commerce-order-checkout-form__postcode-validation-order"></div>',
    ];

    // Город получения.
    $form['wrapper']['container_recipient_address']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City of receipt'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['city'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('City')],
      '#required' => FALSE,
      '#suffix' => '<div class="site-commerce-order-checkout-form__city-validation-order"></div>',
    ];

    // Улица.
    $form['wrapper']['container_recipient_address']['street'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['street'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Street')],
      '#required' => FALSE,
    ];

    // Дом.
    $form['wrapper']['container_recipient_address']['house_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('House'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['house_number'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('House')],
      '#required' => FALSE,
    ];

    // Квартира.
    $form['wrapper']['container_recipient_address']['flat_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Flat'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['flat_number'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Flat')],
      '#required' => FALSE,
    ];

    // Поле фамилия и имя по паспорту.
    $form['wrapper']['container_recipient_address']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full name on passport'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['name'],
      '#description' => $this->t('Full surname, first name and patronymic may be required upon receipt of the order'),
      '#attributes' => ['placeholder' => $this->t('Full name on passport') . ' *'],
      '#required' => TRUE,
      '#suffix' => '<div class="site-commerce-order-checkout-form__name-validation-order"></div>',
    ];

    // Поле телефон.
    $form['wrapper']['container_recipient_address']['phone_mobile'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['phone_mobile'],
      '#required' => TRUE,
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Phone') . ' *'],
      '#suffix' => '<div class="site-commerce-order-checkout-form__phone-mobile-validation-order"></div>',
    ];

    // Поле e-mail.
    $form['wrapper']['container_recipient_address']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail address'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#default_value' => $this->default_order_information['mail'],
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('E-mail address') . ' *'],
      '#suffix' => '<div class="site-commerce-order-checkout-form__mail-validation-order"></div>',
    ];

    // Способ получения.
    $form['wrapper']['container_delivery'] = [
      '#type' => 'container',
      '#prefix' => '<div class="site-commerce-order-checkout-form__container">',
      '#suffix' => '</div>',
    ];
    $form['wrapper']['container_delivery']['delivery_title'] = [
      '#markup' => '<div class="site-commerce-order-checkout-form__title site-commerce-order-checkout-form__delivery-title">' . $this->t('Way getting') . '</div>',
    ];
    $delivery_types = [];
    if (!empty(\Drupal::hasService('plugin.manager.site_commerce_delivery.type'))) {
      $plugin_service = \Drupal::service('plugin.manager.site_commerce_delivery.type');
      foreach ($plugin_service->getDefinitions() as $plugin_id => $plugin) {
        $instance = $plugin_service->createInstance($plugin_id);

        // Если разрешено отображать способ доставки.
        if ($instance->getAvailableStatus()) {
          $delivery_types[$plugin_id] = $instance->getLabel();
        }
      }
    }
    $form['wrapper']['container_delivery']['delivery_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Way getting'),
      '#title_display' => 'invisible',
      '#default_value' => empty($this->default_order_information['delivery_type']) ? 'courier' : $this->default_order_information['delivery_type'],
      '#options' => $delivery_types,
    ];

    // Способы оплаты и правила платежных систем.
    $form['wrapper']['container_payment'] = [
      '#type' => 'container',
      '#prefix' => '<div class="site-commerce-order-checkout-form__container">',
      '#suffix' => '</div>',
    ];

    $form['wrapper']['container_payment']['payment_title'] = [
      '#markup' => '<div class="site-commerce-order-checkout-form__title site-commerce-order-checkout-form__payment-title">' . $this->t('Payment method') . '</div>',
    ];
    $rules_payment_systems = [];
    $payment_methods = [];
    if (!empty(\Drupal::hasService('plugin.manager.site_payments.payment_system'))) {
      $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
      foreach ($plugin_service->getDefinitions() as $plugin_id => $plugin) {
        $instance = $plugin_service->createInstance($plugin_id);

        // Если разрешено отображать способ оплаты.
        if ($instance->getAvailableStatus()) {
          $payment_methods[$plugin_id] = $instance->getPaymentMethodName();

          // Правила платежных систем.
          $rules = $instance->getRulesPaymentSystem();
          if ($rules) {
            $rules_payment_systems[$plugin_id] = $rules;
          }
        }
      }
    }
    $form['wrapper']['container_payment']['payment_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment method'),
      '#title_display' => 'invisible',
      '#default_value' => $this->default_order_information['payment_method'],
      '#options' => $payment_methods,
      '#weight' => 0,
    ];
    foreach ($rules_payment_systems as $plugin_id => $value) {
      $form['wrapper']['container_payment']['rules_' . $plugin_id] = [
        '#type' => 'item',
        '#states' => [
          'visible' => [
            [
              [':input[name="payment_method"]' => ['value' => $plugin_id]],
            ],
          ],
        ],
        '#weight' => 10,
      ];
      $form['wrapper']['container_payment']['rules_' . $plugin_id] += $rules_payment_systems[$plugin_id];
    }

    // Дополнительная информация.
    $form['wrapper']['container_additionally'] = [
      '#type' => 'container',
      '#prefix' => '<div class="site-commerce-order-checkout-form__container">',
      '#suffix' => '</div>',
    ];
    $form['wrapper']['container_additionally']['additionally_title'] = [
      '#markup' => '<div class="site-commerce-order-checkout-form__title">' . $this->t('Additionally') . '</div>',
    ];
    $form['wrapper']['container_additionally']['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment on the order'),
      '#title_display' => 'invisible',
      '#description' => '',
      '#attributes' => ['placeholder' => $this->t('Comment on the order')],
      '#required' => FALSE,
      '#rows' => 5,
    ];

    // Добавляем кнопку для отправки.
    $form['wrapper']['actions']['#type'] = 'actions';
    $form['wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm order'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
      ],
      '#attributes' => ['class' => ['site-commerce-order-checkout-form__btn']],
      '#suffix' => '<div class="site-commerce-order-checkout-form__submit-label"></div>',
      '#access' => count($payment_methods) ? TRUE : FALSE,
    ];

    // Элемент для вывода сообщений об ошибках и уведомлений.
    $form['wrapper']['message'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'site-commerce-order-checkout-form__message',
      ],
    ];

    // Соглашение об обработке персональных данных.
    $config = $this->config('kvantstudio.settings');
    if ($data_policy_information = $config->get('text_data_policy')) {
      $form['wrapper']['container_data_policy'] = [
        '#type' => 'container',
        '#weight' => 100,
        '#prefix' => '<div class="site-commerce-order-checkout-form__data-policy">',
        '#suffix' => '</div>',
      ];

      $data_policy_information = str_replace("@submit_label", $this->t('Confirm order'), $data_policy_information);
      $nid = $config->get('node_agreement_personal_data');
      $data_policy_information = str_replace("@data_policy_url", "/node/" . $nid, $data_policy_information);
      $form['wrapper']['container_data_policy']['data_policy_information'] = [
        '#markup' => $data_policy_information,
      ];
    }

    $form['#attached']['library'][] = 'site_commerce_order/checkout_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Проверка E-mail адреса.
    $mail = trim($form_state->getValue('mail'));
    if ($mail && !\Drupal::service('email.validator')->isValid($mail)) {
      $form_state->setErrorByName('mail', $this->t('You do not specify the correct e-mail.'));
    }

    // Проверка наличия возможности оплаты заказа.
    // Способ оплаты.
    $payment_method = $form_state->getValue('payment_method');
    if ($payment_method) {
      if (!empty(\Drupal::hasService('plugin.manager.site_payments.payment_system'))) {
        $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
        if ($plugin_service->hasDefinition($payment_method)) {
          // Инициализация плагина оплаты.
          $plugin_instance = $plugin_service->createInstance($payment_method);

          // Стоимость заказа.
          $order_cost = $this->databaseOrder->getOrderCost();

          if (!$plugin_instance->checkUserBalance($order_cost)) {
            $form_state->setErrorByName('payment_method', $this->t('There are not enough funds to pay for the order. Choose a different payment method.'));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Оформление заказа.
    $order = '';

    // Настройки оформления заказов.
    $config = \Drupal::config('site_commerce_order.settings');

    // Если есть не оформленные в заказ позиции в корзине.
    if ($this->databaseOrder->hasPositionsInCart()) {
      // Способ оплаты.
      $payment_method = $form_state->getValue('payment_method');

      // Форматируем номер телефона.
      $phone = trim($form_state->getValue('phone_mobile'));
      $phone = \Drupal::service('kvantstudio.formatter')->phone($phone);

      // Регистрация заказа.
      $data = [
        'country' => trim($form_state->getValue('country')),
        'region' => trim($form_state->getValue('region')),
        'postcode' => trim($form_state->getValue('postcode')),
        'city' => trim($form_state->getValue('city')),
        'delivery_type' => $form_state->getValue('delivery_type'),
        'street' => trim($form_state->getValue('street')),
        'house_number' => trim($form_state->getValue('house_number')),
        'flat_number' => trim($form_state->getValue('flat_number')),
        'name' => trim($form_state->getValue('name')),
        'phone_mobile' => $phone,
        'mail' => trim($form_state->getValue('mail')),
        'comment' => trim($form_state->getValue('comment')),
        'payment_method' => $payment_method,
      ];
      $order = $this->databaseOrder->createOrder($data);
    }

    // Если заказ успешно создан.
    if ($order instanceof OrderInterface) {
      // Уникальный идентификатор заказа.
      $order_uuid = $order->uuid();

      // Устанавливаем номер заказа в параметры формы.
      $form_state->setValue('order_uuid', $order_uuid);

      // Формируем идентификатор покупателя.
      // При необходимости выполняем регистрацию учетной записи.
      $uid = \Drupal::currentUser()->id();
      $account = \Drupal\user\Entity\User::load($uid);
      if ($account->isAnonymous()) {
        $registration_controller = \Drupal::service('site_registration.user_registration_controller');

        // Если пользователь не авторизован. Выполняем проверку наличия регистрации
        // по указанным e-mail и номеру телефона.
        $user_exist_account = $registration_controller->checkUserByName($data['mail']);
        if (!$user_exist_account instanceof \Drupal\user\Entity\User) {
          $user_exist_account = $registration_controller->checkUserByName($data['phone_mobile']);
        }

        // Если не удалось загрузить аккаунт, создаем новый.
        if ($user_exist_account instanceof \Drupal\user\Entity\User) {
          $account = $user_exist_account;
        } else {
          // Регистрация учетной записи.
          $customer = explode(' ', $order->getCustomer());
          $data_registration = [
            'field_email_office' => $data['mail'],
            'field_phone_mobile' => $data['phone_mobile'],
            'field_surname' => isset($customer[0]) ? trim($customer[0]) : '',
            'field_name' => isset($customer[1]) ? trim($customer[1]) : '',
            'field_patronymic' => isset($customer[2]) ? trim($customer[2]) : '',
          ];
          $account = $registration_controller->createUser($data['mail'], $data_registration, FALSE);

          // Если пользователь существует выполняем привязку заказа к учетной записи.
          if ($account instanceof User) {
            $order->setOwner($account);
            $order->setCreator($account);
            $order->save();
          }
        }
      }

      // Выполняем вызов плагина выбранной платежной системы.
      if (!empty(\Drupal::hasService('plugin.manager.site_payments.payment_system'))) {
        $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');
        if ($plugin_service->hasDefinition($payment_method)) {
          // Инициализация плагина оплаты.
          $plugin_instance = $plugin_service->createInstance($payment_method);

          // Стоимость заказа.
          $number = $order->getTotalPaidNumber(FALSE);

          // Регистрируем транзакцию и получаем URL для оплаты.
          $payment_data = [
            'uid' => $account->id(),
            'new_customer' => $order->isNewCustomer(),
            'order_id' => $order->id(),
            'order_uuid' => $order_uuid,
            'number' => $number,
            'currency' => $order->getTotalPaidСurrencyСode(),
            'payment_type' => 'online_store'
          ];
          $url = $plugin_instance->getPaymentUrl($payment_data);

          // Устанавливаем в параметры формы URL на страницу оплаты заказа или уведомления о заказе.
          $form_state->setValue('confirmed_url', $url);

          // При необходимости обновляем баланс на счету пользователя.
          // Если это реализует платежный плагин.
          $plugin_instance->updateUserBalance($payment_data);
        }
      }

      // Отправка сообщения о новом заказе на почту менеджеру.
      if ($manager_mail = $config->get('order_manager_mail')) {
        // Формируем состав заказа.
        // TODO: Это временная заглушка. Данная функция будет переписана под формат e-mail письма.
        $build = [
          '#theme' => 'site_commerce_order_view',
          '#order_uuid' => $order->uuid(),
        ];
        $text = \Drupal::service('renderer')->renderPlain($build);
        if ($data['comment']) {
          $text .= '<h2 style="margin: 40px 0 20px 0">' . $this->t('Comment on the order') . '</h2>';
          $text .= $data['comment'];
        }

        $fields = [
          'mid' => 0,
          'uid' => $account->id(),
          'form_name' => '',
          'subject' => 'Новый заказ № ' . $order->id() . ' от ' . \Drupal::service('date.formatter')->format($order->getCreatedTime(), 'custom', 'd.m.Y - H:i'),
          'name' => $order->getCustomer(),
          'phone' => $data['phone_mobile'],
          'mail' => $data['mail'],
          'text' => $text,
          'data' => json_encode([]),
          'status' => 0,
          'created' => $order->getCreatedTime(),
        ];
        site_send_message_mail_send($fields, $manager_mail);
      }
    }
  }

  /**
   * Сохранение данных на сервере.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Выводим ошибки валидации формы.
    $messages = $this->messenger()->all();
    $build = '';
    if (!empty($messages)) {
      $this->messenger()->deleteAll();
      $build = [
        '#theme' => 'status_messages',
        '#message_list' => $messages,
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
      ];
    }

    if ($form_state->hasAnyErrors()) {
      // Обновление формы.
      $response->addCommand(new ReplaceCommand('#site-commerce-order-checkout-form__wrapper', $form['wrapper']));

      $response->addCommand(new ReplaceCommand('#site-commerce-order-checkout-form__message', $build));
    } else {
      // Выполняем редирект на страницу уведомления об успешном заказе или страницу оплаты.
      $confirmed_url = $form_state->getValue('confirmed_url');
      if ($confirmed_url) {
        $command = new RedirectCommand($confirmed_url);
        $response->addCommand($command);
      }
    }

    return $response;
  }
}
