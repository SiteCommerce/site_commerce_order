<?php

namespace Drupal\site_commerce_order;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Order entity.
 *
 * @see \Drupal\site_commerce_order\Entity\Order.
 */
class OrderAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\site_commerce_order\Entity\OrderInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view site_commerce_order entities');

      case 'edit':

        return AccessResult::allowedIfHasPermission($account, 'edit site_commerce_order entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete site_commerce_order entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'create site_commerce_order entities');
  }
}
