<?php

namespace Drupal\site_commerce_order;

use Drupal\site_commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Order entities.
 *
 * @ingroup site_commerce_order
 */
class OrderListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Order number');
    $header['customer'] = $this->t('Customer');
    $header['total_paid_number'] = $this->t('Total paid');
    $header['total_paid_currency'] = $this->t('Currency');
    $header['status'] = $this->t('Order status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\site_commerce_order\Entity\Order $entity */
    $row['id'] = $entity->id();
    $row['customer'] = $entity->getCustomer();
    $row['total_paid_number'] = $entity->getTotalPaidNumber();
    $row['total_paid_currency'] = $entity->getTotalPaidCurrencySymbol();
    $row['status'] = $entity->getStatus();
    // $row['name'] = Link::createFromRoute(
    //   $entity->label(),
    //   'entity.site_commerce_order.edit_form',
    //   ['site_commerce_order' => $entity->id()]
    // );
    return $row + parent::buildRow($entity);
  }

  /**
   * Gets this list's default operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operations are for.
   *
   * @return array
   *   The array structure is identical to the return value of
   *   self::getOperations().
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 0,
        'url' => $this->ensureDestination($entity->toUrl('canonical')),
      ];
    }
    if ($entity->access('update') && $entity->hasLinkTemplate('edit-form')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('edit-form')),
      ];
    }
    if ($entity->access('delete') && $entity->hasLinkTemplate('delete-form')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'weight' => 100,
        'url' => $this->ensureDestination($entity->toUrl('delete-form')),
      ];
    }

    return $operations;
  }

}
