<?php

namespace Drupal\site_commerce_order\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the price order formatter.
 *
 * @FieldFormatter(
 *   id = "site_commerce_order_price",
 *   label = @Translation("Order price"),
 *   field_types = {
 *     "site_commerce_price"
 *   }
 * )
 */
class Price extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $number = \Drupal::service('kvantstudio.formatter')->price($item->number);
      $manager = \Drupal::entityTypeManager()
      ->getStorage('site_commerce_currency')
      ->loadByProperties(['letter_code' => $item->currency_code]);
      $currency = reset($manager);

      $elements[$delta] = [
        '#markup' => $number . ' ' . $currency->getSymbol(),
      ];
    }

    return $elements;
  }

}
