<?php

/**
 * @file
 * Contains \Drupal\site_commerce_order\Plugin\Field\FieldFormatter\Status.
 */

namespace Drupal\site_commerce_order\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the order status formatter.
 *
 * @FieldFormatter(
 *   id = "site_commerce_order_status",
 *   label = @Translation("Order status"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class Status extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => site_commerce_order_statuses($item->value),
      ];
    }

    return $elements;
  }

}
