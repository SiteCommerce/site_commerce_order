<?php

namespace Drupal\site_commerce_order\TwigExtension;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class OrderTwigExtension extends \Twig_Extension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('get_order_checkout_form', [$this, 'getOrderСheckoutForm']),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_commerce_order.twig_extension';
  }

  /**
   * Форма оформления заказа.
   */
  public static function getOrderСheckoutForm() {
    $form = \Drupal::formBuilder()->getForm('Drupal\site_commerce_order\Form\СheckoutForm');
    return $form;
  }
}
